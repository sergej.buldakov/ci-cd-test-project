package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func doubleMessage(in string) string {
	return fmt.Sprintf(`%s %s`, in, in)
}

func main() {
	router := gin.New()
	router.POST(":message", func(context *gin.Context) {
		message := context.Param("message")
		context.AbortWithStatusJSON(http.StatusOK, gin.H{"message": doubleMessage(message)})
	})

	panic(router.Run(":8543"))
}
